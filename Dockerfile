FROM ubuntu:xenial

ARG USERNAME
ENV USERNAME=${USERNAME:-user}
ARG USER_ID
ENV USER_ID=${USER_ID:-1000}
ARG GROUP_ID
ENV GROUP_ID=${GROUP_ID:-${USER_ID}}

RUN apt-get update \
    && apt-get install -y \
        ssh \
        build-essential \
        gcc \
        g++ \
        gdb \
        clang \
        cmake \
        rsync \
        tar \
        python \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir /var/run/sshd \
    && groupadd --gid ${GROUP_ID} ${USERNAME} \
    && useradd -m -s /bin/bash --uid ${USER_ID} --gid ${GROUP_ID} ${USERNAME} \
    && yes password | passwd ${USERNAME} \
    && mkdir /code \
    && chown ${USERNAME}:${USERNAME} /code

COPY bin/ssh.sh /usr/local/bin/ssh.sh

WORKDIR /code
EXPOSE 22

CMD ["/usr/local/bin/ssh.sh"]
