#!/usr/bin/env bash

_=$(id ${USERNAME} > /dev/null)
USER_MISSING=$?
if [ "x${USER_MISSING}" == "x1" ]; then
  groupadd --gid ${GROUP_ID} ${USERNAME}
  useradd -m -s /bin/bash --uid ${USER_ID} --gid ${GROUP_ID} ${USERNAME}
  yes password | passwd ${USERNAME}
fi

if [ -z "${USER_PASSWORD}" ]; then
  yes "${USER_PASSWORD}" | passwd ${USERNAME}
fi

mkdir -p ~${USERNAME}/.ssh
touch ~${USERNAME}/.ssh/authorized_keys

if [ ! -z "${AUTHORIZED_KEYS_URL}" ]; then
  wget "${AUTHORIZED_KEYS_URL}" -O ~${USERNAME}/.ssh/authorized_keys
fi

if [ ! -z "${AUTHORIZED_KEYS}" ]; then
  echo "${AUTHORIZED_KEYS}" >> ~${USERNAME}/.ssh/authorized_keys
fi

chown -R ${USERNAME}:${USERNAME} ~${USERNAME}/.ssh
chmod 0700 ~${USERNAME}/.ssh
chmod 0600 ~${USERNAME}/.ssh/authorized_keys
chown ${USERNAME}:${USERNAME} /code

/usr/sbin/sshd -D
