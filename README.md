# Docker for use in OpenU 20465 course

For the Israeli OpenU course 20465 - Systems Programming Lab the GCC available in Ubuntu 16.04 is required.

This project provides a docker image that can be used with CLion (and maybe other IDEs).

The docker image can be used for simple compiling using a unified GCC version or debug in the IDE without the need of a virtual machine.

## Image sources:

* GitLab: `registry.gitlab.com/uda/openu-20465-docker`
* Docker Hub: `openu/clang-20465` (This is the default in examples and code)

## Image versions

* master - Latest successful build from the `master` branch
* latest - Latest successful build from a git tag
* TAG - A successful build from an arbitrary TAG

ALL VERSION have a 32bit version as `{VERSION}-32bit`, i.e. `master` and `master-32bit`, `latest` and `latest-32bit`.
The examples here assume you are using the 32bit since that is the requirement in the course.

## Just compile

Instead of running `gcc` installed in your OS, run the gcc provided by the docker image, use the following command:

```shell
docker run --rm --user "$UID:$GID" -v `pwd`:/code openu/clang-20465:latest-32bit gcc -ansi -pedantic -g -Wall hello.c -o hello
```

Adapt the `gcc` flags as needed.

You can also use an alias for clearer command line, add it to your `~/.bashrc` or `~/.zshrc` or what ever file you add your login scripts.

```shell
alias dgcc='docker run --rm --user "$UID:$GID" -v `pwd`:/code openu/clang-20465:latest-32bit gcc'
# Then you can run
dgcc -ansi -pedantic -g -Wall hello.c -o hello
```

## Usage with IDE

For all steps you will need to install the `docker` executable: [https://docs.docker.com/get-docker/](https://docs.docker.com/get-docker/).

All documentation from here on assumes you are using Linux, information for other OSs might be added in the future.

### 1a. docker (vanilla)

Just run the following command:

```shell
docker run -d --cap-add sys_ptrace -p127.0.0.1:2222:22 --name openu-clang-20465 openu/clang-20465:latest-32bit
```

### 1b. docker-compose

You will need to install `docker-compose`: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/).

You can copy the [`docker-compose.yml`](docker-compose.yml) to your project directory or clone this project locally.

Then run:

```shell
docker-compose up -d
```

### 2. Using the remote host

Follow the instructions here:

https://blog.jetbrains.com/clion/2020/01/using-docker-with-clion/#using-the-remote-development-workflow-with-docker

Remember to move the toolchain up for it to become the default.

## Configuration

The script running the SSH daemon respects some environment variables that can change the usage.

_NOTICE: the context `docker-compose up` implies `docker run`, but not the opposite._

| Name | Context | Notes |
| --- | --- | --- |
| USERNAME | `docker build`, `docker run` | Sets the user name to be used for login, default: `user` |
| USER_ID | `docker build`, `docker run` | Sets the login user ID, required if you want to mount the working directory to /code, default: `1000` |
| GROUP_ID | `docker build`, `docker run` | Sets the login user group ID, required if you want to mount the working directory to /code, default: `1000` |
| USER_PASSWORD | `docker run` | Set a password other than the default "`password`" |
| AUTHORIZED_KEYS_URL | `docker run` | Fetch the contents of `~/.ssh/authorized_keys` from a URL, example: `https://gitlab.com/{user}.keys` or `https://github.com/{user}.keys`, this is overriden by `AUTHORIZED_KEYS` if set |
| AUTHORIZED_KEYS | `docker run` | An SSH key or keys as a string, This overrides the contents that might be set using `AUTHORIZED_KEYS_URL` |
| SSH_LISTEN_IP | `docker-compose up` | Set the listening IP, this is not necessary unless running it on another machine than the IDE, default: `127.0.0.1` |
| SSH_LISTEN_PORT | `docker-compose up` | Set the listening port, necessary if you already have something listening on port `2222`, default: `2222` |
